# Fractal Explorer



## Description

This is a school project made by Gaetan Basile and Lucas Dumartin.

It draws a set of Fractals. To be more precise only the Julia set, the Newton fractal and the Mandelbrot set are implemented. They are drawn using multithreading to share the load throught the number of cores you've got.

The project was made with Java 8 and JavaFX.