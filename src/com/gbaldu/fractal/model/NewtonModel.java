package com.gbaldu.fractal.model;

import com.gbaldu.fractal.tool.Complex;

/**
 * The NewtonModel class inherits from JuliaModel most of its functionality.
 * However, because of the Newton fractal's special nature, the model needs to encompass new attributes.
 */
public class NewtonModel extends JuliaModel
{
    /**
     * The results of the computation of the roots
     * A double array is used for thread safety.
     */
    public int[][] rootResults;
    /**
     * The solutions to the roots of the fractal's equation.
     */
    private Complex[] rootSolutions;

    /**
     * Constructor that must be used when making a new instance of NewtonModel.
     *
     * @param type The type of the fractal that will be represented by this model.
     */
    public NewtonModel(FractalType type)
    {
        super(type);
        rootSolutions = new Complex[3];

        rootSolutions[0] = new Complex(1, 0);
        rootSolutions[1] = new Complex(-.5, Math.sqrt(3) / 2);
        rootSolutions[2] = new Complex(-.5, -Math.sqrt(3) / 2);

        setColorOffset(1);
    }

    /**
     * Returns a root of the fractal's equation
     *
     * @param i The index of the solution to get in the array
     * @return The solution as a Complex
     */
    public Complex getNewtonsPoints(int i)
    {
        if (i >= 0 && i <= 2)
            return rootSolutions[i];
        return null;
    }

    /**
     * Returns the complexity of the Newton fractal.
     * @return The value of the complexity.
     */
    public int getComplexity()
    {
        return 3;
    }
}
