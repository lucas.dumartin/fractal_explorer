package com.gbaldu.fractal.model;

/**
 * The FractalType enum represents the different fractals available in the program.
 * It also provides some utility to make the fractal switcher more efficient.
 * @author Gaétan Basile
 */
public enum FractalType
{

    MANDELBROT,
    JULIA,
    NEWTON;

    /**
     * This method is used to get the string value a a FractalType object.
     *
     * @return The name of the fractal represented by the enum
     */
    @Override
    public String toString()
    {
        switch (this)
        {
            case MANDELBROT:
                return "Mandelbrot";
            case JULIA:
                return "Julia";
            case NEWTON:
                return "Newton";
            default:
                return "Rien";
        }
    }
}
