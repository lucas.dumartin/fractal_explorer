package com.gbaldu.fractal.model;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

/**
 * The JuliaModel class inherits most of its attributes from FractalModel.
 * However, it stores two more attributes that make it possible to change some parameters.
 */
public class JuliaModel extends FractalModel
{
    /**
     * The alpha parameter, is used to change the appearance of the fractal
     */
    private DoubleProperty alpha = new SimpleDoubleProperty(0.285);
    /**
     * The beta parameter, is used to change the appearance of the fractal
     */
    private DoubleProperty beta = new SimpleDoubleProperty(0.01);


    /**
     * Constructor that must be used when making a new instance of JuliaModel.
     *
     * @param type The type of the fractal that will be represented by this model.
     */
    public JuliaModel(FractalType type)
    {
        super(type);
    }

    /**
     * Getter for the beta property
     * @return The value of beta
     */
    public double getBeta()
    {
        return beta.get();
    }

    /**
     * Setter for the beta property
     *
     * @param beta The new value to use a beta parameter
     */
    public void setBeta(double beta)
    {
        this.beta.set(beta);
    }

    /**
     * Getter for the beta property itself
     * @return The beta property
     */
    public DoubleProperty betaProperty()
    {
        return beta;
    }

    /**
     * Getter for the alpha property
     * @return The value of alpha
     */
    public double getAlpha()
    {
        return alpha.get();
    }

    /**
     * Setter for the alpha property
     * @param alpha The new value to use a alpha parameter
     */
    public void setAlpha(double alpha)
    {
        this.alpha.set(alpha);
    }

    /**
     * Getter for the alpha property itself
     *
     * @return The alpha property
     */
    public DoubleProperty alphaProperty()
    {
        return alpha;
    }
}
