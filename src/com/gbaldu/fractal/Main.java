package com.gbaldu.fractal;

import com.gbaldu.fractal.view.MainView;

import javafx.application.Application;

public class Main {
    public static void main(String[] args) {
        Application.launch(MainView.class, args);
    }
}
