package com.gbaldu.fractal.view.pane;

import com.gbaldu.fractal.controller.FractalController;
import com.gbaldu.fractal.model.FractalModel;
import com.gbaldu.fractal.model.JuliaModel;

import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.Slider;
import javafx.scene.image.WritableImage;

/**
 * The JuliaPane class inherits from FractalPane most of its functionality but enables changing parameters via Sliders.
 */
public class JuliaPane extends FractalPane
{
    private Slider alphaSlider = new Slider(0, 1, 0.285);
    private Slider betaSlider = new Slider(0, 1, 0.01);

    /**
     * Initialisation of a Julia fractal view
     * @param model The model which will be represented by this view
     * @param controller The controller associated with this model
     * @throws IllegalArgumentException When model is not a JuliaModel
     */
    public JuliaPane(FractalModel model, FractalController controller) throws IllegalArgumentException
    {
        super(model, controller);
        if (!(model instanceof JuliaModel))
            throw new IllegalArgumentException("Le modèle n'est pas un JuliaModel");

        this.initActionButtons();
        this.initSliders();
        this.initSphereMode();
        this.initGenerationStatistics();
    }

    /**
     * Initialisation of the sliders that modulate the Julia's parameters
     */
    private void initSliders()
    {
        alphaSlider.valueProperty().bindBidirectional(((JuliaModel) fractalModel).alphaProperty());
        betaSlider.valueProperty().bindBidirectional(((JuliaModel) fractalModel).betaProperty());

        menu.getItems().addAll(
                new Separator(),
                new Label("Alpha :"),
                alphaSlider,
                new Label("Beta :"),
                betaSlider
        );
        alphaSlider.valueProperty().addListener(observable -> fractalController.launchCalc());
        betaSlider.valueProperty().addListener(observable -> fractalController.launchCalc());
    }

    /**
     * Initialisation of the modified actions on the reset Button which needs to reset alpha and beta too
     */
    @Override
    protected void initActionButtons()
    {
        super.initActionButtons();

        reset.setOnAction(event ->
        {
            fractalModel.writableImage = new WritableImage(fractalModel.getWidth(), fractalModel.getHeight());
            backgroundImageView.setImage(null);
            imageView.setImage(fractalModel.writableImage);

            fractalModel.setGenerationStatus(0.);
            ((JuliaModel) fractalModel).setAlpha(0.285);
            ((JuliaModel) fractalModel).setBeta(0.01);
            fractalController.reloadModel();
        });
    }
}
