package com.gbaldu.fractal.view.pane;

import com.gbaldu.fractal.model.FractalModel;
import com.gbaldu.fractal.tool.Complex;

import javafx.geometry.Point3D;
import javafx.scene.AmbientLight;
import javafx.scene.layout.Pane;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Sphere;
import javafx.scene.transform.Rotate;

/**
 * The SphericalPane class is a basic view of a fractal, projected on a 3D sphere.
 * It does not provide any supplementary controls, as most actions are done by clicking on the sphere.
 * Inheriting from pane enables it to be used as a view pane alone, or in a FractalPane to have the standard fractal controls.
 */
public class SphericalPane extends Pane
{
    private PhongMaterial material = new PhongMaterial();
    private Sphere sphere = new Sphere();
    private FractalModel fractalModel;

    private Complex initialMousePosition = new Complex(0, 0);
    private Complex lastRotation = new Complex(0, 0);
    private Complex angle = new Complex(0,0);

    /**
     * view of a fractal applies on a Sphere
     * @param model The model which will be represented by this spherical view
     */
    SphericalPane(FractalModel model)
    {
        this.fractalModel = model;

        sphere.setMaterial(material);
        getChildren().add(sphere);
        getChildren().add(new AmbientLight());

        setOnMousePressed(event ->
        {
            lastRotation.real = angle.real;
            lastRotation.imaginary = angle.imaginary;

            initialMousePosition.real = event.getX();
            initialMousePosition.imaginary = event.getY();
        });

        setOnMouseDragged(event ->
        {
            sphere.getTransforms().clear();
            angle.real = (initialMousePosition.real - event.getX()) / 8.;
            angle.imaginary = (event.getY() - initialMousePosition.imaginary) / 8.;
            angle.real += lastRotation.real;
            angle.imaginary += lastRotation.imaginary;
            sphere.getTransforms().add(new Rotate(angle.imaginary, new Point3D(1, 0, 0)));
            sphere.getTransforms().add(new Rotate(angle.real, new Point3D(0, 1, 0)));
        });
    }

    /**
     * Method to invoke when the size of the window is changed.
     * Change the Sphere's Size, position
     * And reload the image from the model
     * @param width The new width
     * @param height The new height
     */
    public void init(int width, int height)
    {
        int radius = Math.min(width, height) / 2 - 80;

        sphere.setTranslateX(width / 2.);
        sphere.setTranslateY(height / 2.);

        changeImage();

        sphere.setRadius(radius);
    }

    /**
     * When a new Image is created in the model, this method is used to apply it to the Sphere
     */
    void changeImage()
    {
        material.setDiffuseMap(fractalModel.writableImage);
    }
}
