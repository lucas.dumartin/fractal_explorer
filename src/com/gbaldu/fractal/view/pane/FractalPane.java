package com.gbaldu.fractal.view.pane;

import java.io.File;
import java.io.IOException;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import com.gbaldu.fractal.controller.FractalController;
import com.gbaldu.fractal.model.FractalModel;
import com.gbaldu.fractal.model.FractalType;
import com.gbaldu.fractal.tool.Complex;

import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Separator;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ToolBar;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.util.StringConverter;
import javafx.util.converter.DoubleStringConverter;

/**
 * The FractalPane class is an all in one view a fractal.
 * It provides basic controls and functionality and can be inherited from for more advanced fractals.
 * Inheriting from BorderPane enables it to have a simple yet effective layout control.
 */
public class FractalPane extends BorderPane
{
    /**
     * A constant which contains the width of the menu ribbon.
     */
    public static final int MENU_WIDTH = 150;
    /**
     * A pane containing the spherical view of the fractal.
     */
    public SphericalPane sphere;
    /**
     * The model represented by this view.
     */
    FractalModel fractalModel;
    /**
     * The controller associated with the model.
     */
    FractalController fractalController;
    /**
     * The first image display, which will display the fractal.
     */
    ImageView imageView;
    /**
     * The second image display, which is used for double-buffering.
     */
    ImageView backgroundImageView;
    /**
     * The menu ribbon containing all the scene controls.
     */
    ToolBar menu = new ToolBar();
    /**
     * A Button used to reset the view, iterations, and to do a new computation.
     */
    Button reset = new Button("Reset");
    /**
     * A Complex which is used to store the mouse's position upon a zoom.
     */
    private Complex mousePositionOnZoom = new Complex(0, 0);
    /**
     * A Label which is used to display calculations time and draw time.
     */
    private Label generationStatistics = new Label();
    /**
     * A pane containing the two images.
     */
    private Pane images = new Pane();
    /**
     * An input for the number of iterations for the computation.
     */
    private TextField iterationsInput;
    /**
     * Input for the translation along the X axis.
     */
    private TextField xInput = new TextField();
    /**
     * Input for the translation along the Y axis.
     */
    private TextField yInput = new TextField();
    /**
     * A Button used to regenerate the image. Does not reset parameters.
     */
    private Button regenerate = new Button("Regenerate");
    /**
     * The starting position of the view.
     */
    private Complex initPos = new Complex(0, 0);
    /**
     * A selector for choosing the fractal to display.
     */
    private ToggleGroup fractalSelector = new ToggleGroup();
    /**
     * A checkbox used to toggle the 3D spherical representation mode.
     */
    private CheckBox displaySphere = new CheckBox("Sphere mode ");

    /**
     * Initialisation of a fractal view
     * @param model The model which will be represented by this view
     * @param controller The controller associated with this model
     */
    public FractalPane(FractalModel model, FractalController controller)
    {
        super();

        autosize();

        this.fractalModel = model;
        this.fractalController = controller;

        ///the actual initialisation is done in many simple methods
        this.initImageView();
        this.initMenu();

        ///due to Julia and Newton fractals having different controls in the menu,
        // this has to be done only for mandelbrot
        if (fractalModel.getFractalType() == FractalType.MANDELBROT)
        {
            this.initActionButtons();
            this.initSphereMode();
            this.initGenerationStatistics();
        }
        this.initKeyboardShortcuts();

        Platform.runLater(() -> images.requestFocus());
    }

    /**
     * Method to invoke when the size of the window is changed.
     * @param width The new width of the window
     * @param height The new height of the window
     */
    public void init(int width, int height)
    {
        fractalController.init(width - MENU_WIDTH, height);

        menu.setPrefWidth(MENU_WIDTH);
        menu.setPrefHeight(height);

        imageView.setImage(fractalModel.writableImage);
        backgroundImageView.setImage(null);
    }

    /**
     * Method which initialises the spherical representation mode.
     */
    void initSphereMode()
    {
        displaySphere.selectedProperty().addListener(observable ->
        {
            if (displaySphere.isSelected())
            {
                sphere = new SphericalPane(fractalModel);
                sphere.init(fractalModel.getWidth(), fractalModel.getHeight());

                sphere.changeImage();
                this.getChildren().remove(menu);
                this.setCenter(sphere);
                this.setLeft(menu);
                menu.toFront();
            } else
            {
                this.getChildren().remove(menu);
                this.setCenter(images);
                this.setLeft(menu);
                menu.toFront();
            }
        });
        menu.getItems().add(displaySphere);
    }

    /**
     * Method which initialises the common controls of the menu.
     */
    private void initMenu()
    {
        ///General menu setup
        menu.setOrientation(Orientation.VERTICAL);
        this.setLeft(menu);

        ///Initialisation of the calculus ProgressBar
        ProgressBar generationStatus = new ProgressBar(0.);
        generationStatus.setPrefWidth(MENU_WIDTH);
        generationStatus.progressProperty().bindBidirectional(fractalModel.generationStatusProperty());

        menu.getItems().addAll(
                generationStatus,
                new Separator(),
                new Label("fractal")
        );

        ///Initialisation of the fractal selection controls
        for (FractalType type : FractalType.values())
        {
            RadioButton rb = new RadioButton(type.toString());
            rb.setUserData(type);
            fractalSelector.getToggles().add(rb);
            if (type == fractalModel.getFractalType())
                rb.setSelected(true);
            menu.getItems().add(rb);
        }

        menu.getItems().add(new Separator());

        fractalSelector.selectedToggleProperty().addListener((observable, oldValue, newValue) ->
        {
            if (fractalSelector.getSelectedToggle() != null)
                fractalController.changeFractalType((FractalType) fractalSelector.getSelectedToggle().getUserData());
        });

        ///Initialisation of the inputs (iterations, scale, X,Y)
        iterationsInput = new TextField();
        iterationsInput.setEditable(true);
        iterationsInput.setTextFormatter(new DoubleTextFormatter(false).formatter);
        iterationsInput.textProperty().bindBidirectional(fractalModel.maxIterationsProperty(), new DoubleStringConverter());
        iterationsInput.textProperty().addListener(observable -> fractalController.launchCalc());
        iterationsInput.textProperty().addListener((observable, oldValue, newValue) ->
        {
            DoubleStringConverter d = new DoubleStringConverter();
            if (d.fromString(newValue) > FractalModel.MAX_ITERATIONS)
                iterationsInput.setText(d.toString(FractalModel.MAX_ITERATIONS));
        });

        menu.getItems().addAll(
                new Label("Iterations"),
                iterationsInput
        );

        /*
        An input for the scale factor of the view.
        */
        TextField scaleInput = new TextField();
        scaleInput.setEditable(true);
        scaleInput.setTextFormatter(new DoubleTextFormatter(false).formatter);
        scaleInput.textProperty().bindBidirectional(fractalModel.scaleProperty(), new DoubleStringConverter());
        scaleInput.textProperty().addListener(observable -> fractalController.launchCalc());

        menu.getItems().addAll(
                new Label("Scale"),
                scaleInput
        );

        xInput.textProperty().bindBidirectional(fractalModel.centerXProperty(), new DoubleStringConverter());
        yInput.textProperty().bindBidirectional(fractalModel.centerYProperty(), new DoubleStringConverter());
        xInput.setTextFormatter(new DoubleTextFormatter(true).formatter);
        yInput.setTextFormatter(new DoubleTextFormatter(true).formatter);
        xInput.textProperty().addListener(observable -> fractalController.launchCalc());
        yInput.textProperty().addListener(observable -> fractalController.launchCalc());

        menu.getItems().addAll(
                new Label("Position"),
                xInput,
                yInput
        );

        ///Initialisation of the color Slider
        Slider colorSlider = new Slider(1, 15, 1);
        colorSlider.valueProperty().bindBidirectional(fractalModel.colorOffsetProperty());
        colorSlider.valueProperty().addListener(observable -> fractalController.launchCalc());
        colorSlider.setSnapToTicks(true);
        colorSlider.setShowTickMarks(true);
        colorSlider.setShowTickLabels(false);
        colorSlider.setMinorTickCount(0);
        colorSlider.setMajorTickUnit(1);
        colorSlider.setSnapToTicks(true);

        menu.getItems().addAll(
                new Separator(),
                new Label("Color"),
                colorSlider
        );
    }

    /**
     * Setup of the keyboard shortcuts for the program
     */
    private void initKeyboardShortcuts()
    {
        setOnKeyPressed(event ->
        {
            switch (event.getCode())
            {
                case RIGHT:
                    fractalController.changeMaxIterations(100);
                    fractalController.launchCalc();
                    break;
                case LEFT:
                    fractalController.changeMaxIterations(-100);
                    fractalController.launchCalc();
                    break;
                case UP:
                    fractalController.changeMaxIterations(10);
                    fractalController.launchCalc();
                    break;
                case DOWN:
                    fractalController.changeMaxIterations(-10);
                    fractalController.launchCalc();
                    break;
                case P:
                    File file = new File("CanvasImage.png");
                    try
                    {
                        ImageIO.write(SwingFXUtils.fromFXImage(fractalModel.writableImage, null), "png", file);
                    } catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                    break;
            }
        });
    }

    /**
     * Initialisation of the image views
     */
    private void initImageView()
    {
        ///General images setup
        imageView = new ImageView();
        imageView.setSmooth(true);
        backgroundImageView = new ImageView();
        backgroundImageView.setSmooth(true);
        images.getChildren().addAll(
                backgroundImageView,
                imageView
        );
        this.setCenter(images);

        ///Event setup
        images.setOnMouseDragged(event ->
        {
            imageView.setTranslateX(event.getX() - initPos.real);
            imageView.setTranslateY(event.getY() - initPos.imaginary);
        });
        images.setOnMouseReleased(event ->
        {
            if (imageView.getTranslateX() != 0 || imageView.getTranslateY() != 0)
            {
                fractalController.stop();
                fractalController.moveTo(-imageView.getTranslateX() / fractalModel.getScale(), -imageView.getTranslateY() / fractalModel.getScale());
                backgroundImageView.setImage(fractalModel.writableImage);
                backgroundImageView.setTranslateX(imageView.getTranslateX());
                backgroundImageView.setTranslateY(imageView.getTranslateY());
                fractalController.setWritableImage(new WritableImage(fractalModel.getWidth(), fractalModel.getHeight()));
                imageView.setImage(fractalModel.writableImage);
                imageView.setTranslateX(0);
                imageView.setTranslateY(0);
                backgroundImageView.setScaleX(1);
                backgroundImageView.setScaleY(1);
                fractalController.launchCalcIn(10);
            }
        });
        images.setOnMousePressed(event ->
        {
            requestFocus();

            initPos.real = (short) event.getX();
            initPos.imaginary = (short) event.getY();
            backgroundImageView.setImage(null);
        });

        images.setOnScroll(event ->
        {
            if (!fractalModel.isZooming)
            {
                setupZoom(event);
            }
            zoomOnCursor(event);
        });

        images.requestFocus();
    }

    /**
     * Initialisation of the "Regenerate" and "Reset" Buttons
     */
    protected void initActionButtons()
    {
        menu.getItems().addAll(
                new Separator(),
                regenerate,
                reset
        );

        reset.setOnAction(event ->
        {
            fractalController.setWritableImage(new WritableImage(fractalModel.getWidth(), fractalModel.getHeight()));
            backgroundImageView.setImage(null);
            imageView.setImage(fractalModel.writableImage);
            fractalController.reloadModel();
            if (sphere != null)
                sphere.changeImage();
        });

        regenerate.setOnAction(event ->
        {
            fractalController.setWritableImage(new WritableImage(fractalModel.getWidth(), fractalModel.getHeight()));
            backgroundImageView.setImage(null);
            imageView.setImage(fractalModel.writableImage);
            fractalController.launchCalcIn(10);
            if (sphere != null)
                sphere.changeImage();
        });
    }

    /**
     * Setup of the calculus statistics
     */
    void initGenerationStatistics()
    {
        generationStatistics.textProperty().bind(fractalModel.generationStatistics);
        menu.getItems().addAll(
                new Separator(),
                generationStatistics
        );
    }

    /**
     * Preparation work for zooming
     *
     * @param event the scrolling event which triggered the setOnScroll listener
     */
    private void setupZoom(ScrollEvent event)
    {
        mousePositionOnZoom.real = event.getX();
        mousePositionOnZoom.imaginary = event.getY();
        fractalController.setIsZooming(true);
        fractalController.stop();
        backgroundImageView.setImage(fractalModel.writableImage);
        backgroundImageView.setTranslateX(imageView.getTranslateX());
        backgroundImageView.setTranslateY(imageView.getTranslateY());
        fractalController.setWritableImage(new WritableImage(fractalModel.getWidth(), fractalModel.getHeight()));
        imageView.setImage(fractalModel.writableImage);
        imageView.setTranslateX(0);
        imageView.setTranslateY(0);
        backgroundImageView.setScaleX(1);
        backgroundImageView.setScaleY(1);
    }

    /**
     * Method used to change the parameters automatically to simulate a zoom action
     * @param event the scrolling event which trippe the setOnScroll listener
     */
    private void zoomOnCursor(ScrollEvent event)
    {
        double oldBackgroundScale = backgroundImageView.getScaleY();
        double originalMouseX = (mousePositionOnZoom.real - fractalModel.getHalfWidth()) / (fractalModel.getScale());
        double originalMouseY = (mousePositionOnZoom.imaginary - fractalModel.getHalfHeight()) / (fractalModel.getScale());
        if (event.getDeltaY() > 0)
        {
            fractalController.changeMaxIterations(1);
            fractalController.setScale(fractalModel.getScale() * fractalModel.scaleIncrement);
            backgroundImageView.setScaleX(backgroundImageView.getScaleX() * fractalModel.scaleIncrement);
            backgroundImageView.setScaleY(backgroundImageView.getScaleY() * fractalModel.scaleIncrement);
        } else
        {
            if (fractalModel.getScale() > 1)
            {
                fractalController.changeMaxIterations(-1);
                fractalController.setScale(fractalModel.getScale() / fractalModel.scaleIncrement);
                backgroundImageView.setScaleX(backgroundImageView.getScaleX() / fractalModel.scaleIncrement);
                backgroundImageView.setScaleY(backgroundImageView.getScaleY() / fractalModel.scaleIncrement);
            }
        }
        double mouseX = (mousePositionOnZoom.real - fractalModel.getHalfWidth()) / (fractalModel.getScale());
        double mouseY = (mousePositionOnZoom.imaginary - fractalModel.getHalfHeight()) / (fractalModel.getScale());

        fractalController.moveToOffset(originalMouseX - mouseX, originalMouseY - mouseY);

        backgroundImageView.setTranslateX(backgroundImageView.getTranslateX() -
                ((originalMouseX - mouseX) * fractalModel.getScale()) * oldBackgroundScale);
        backgroundImageView.setTranslateY(backgroundImageView.getTranslateY() -
                ((originalMouseY - mouseY) * fractalModel.getScale()) * oldBackgroundScale);
        fractalController.launchCalc();
    }

    /**
     * The DoubleTextFormatter class is used to enable Double formatting in a textfield.
     * This is necessary because using listeners on the TextField value does not work
     * as the invalid modified value is sent through the binding before the listener is activated to modify it.
     */
    private class DoubleTextFormatter
    {
        /**
         * A regular expression defining the accepted text format.
         */
        private Pattern validEditingState = Pattern.compile("-?(\\d*)?(\\d\\.\\d*(E\\d{1,2})?)?");
        /**
         * Another regular expression which defines the invalid text format.
         * Complements validEditingState to ensure that incorrect text is not sent through the bindings.
         */
        private Pattern nonValidEditingState = Pattern.compile("-");
        /**
         * The operator which determines whether the entered text is correct or not.
         */
        private UnaryOperator<TextFormatter.Change> filter = change ->
        {
            if (validEditingState.matcher(change.getControlNewText()).matches()
                    && !change.getControlNewText().isEmpty()
                    && !nonValidEditingState.matcher(change.getControlNewText()).matches())
                return change;
            else return null;
        };
        /**
         * A converter to convert from a String to a Double
         */
        private StringConverter<Double> converter = new DoubleStringConverter();
        /**
         * The TextFormatter itself, which will be given to the TextFields
         */
        TextFormatter<Double> formatter = new TextFormatter<>(converter, 0.0, filter);

        /**
         * The default constructor for a DoubleTextFormatter.
         *
         * @param canBeNegative Whether the formatting allows for negative values or not.
         */
        DoubleTextFormatter(boolean canBeNegative)
        {
            if (!canBeNegative)
            {
                nonValidEditingState = Pattern.compile("-(\\d*)?(\\d\\.\\d*(E\\d{1,2})?)?");
            }
        }
    }
}
