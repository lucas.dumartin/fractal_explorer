package com.gbaldu.fractal.view.pane;

import com.gbaldu.fractal.controller.FractalController;
import com.gbaldu.fractal.model.FractalModel;
import com.gbaldu.fractal.model.JuliaModel;
import com.gbaldu.fractal.model.NewtonModel;

import javafx.scene.image.WritableImage;

/**
 * The NewtonPane class inherits its functionality from JuliaPane, but is only compatible with Newton fractals.
 */
public class NewtonPane extends JuliaPane {
    /**
     * Initialisation of a Newton fractal view
     * @param model The model which will be represented by this view
     * @param controller The controller associated with this model
     * @throws IllegalArgumentException When model is not a NewtonModel
     */
    public NewtonPane(FractalModel model, FractalController controller) throws IllegalArgumentException {
        super(model, controller);

        if (!(model instanceof NewtonModel))
            throw new IllegalArgumentException("Le modèle n'est pas un NewtonModel");

    }

    /**
     * Initialisation of the modified actions on the reset Button which needs to reset alpha and beta too
     */
    @Override
    protected void initActionButtons()
    {
        super.initActionButtons();

        reset.setOnAction(event ->
        {
            fractalModel.writableImage = new WritableImage(fractalModel.getWidth(), fractalModel.getHeight());
            backgroundImageView.setImage(null);
            imageView.setImage(fractalModel.writableImage);

            fractalModel.setGenerationStatus(0.);
            ((JuliaModel) fractalModel).setAlpha(1);
            ((JuliaModel) fractalModel).setBeta(0);
            fractalController.reloadModel();
        });
    }
}
