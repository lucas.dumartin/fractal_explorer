package com.gbaldu.fractal.view;


import com.gbaldu.fractal.controller.FractalController;
import com.gbaldu.fractal.controller.impl.JuliaController;
import com.gbaldu.fractal.controller.impl.MandelBrotController;
import com.gbaldu.fractal.controller.impl.NewtonController;
import com.gbaldu.fractal.model.FractalModel;
import com.gbaldu.fractal.model.FractalType;
import com.gbaldu.fractal.model.JuliaModel;
import com.gbaldu.fractal.model.NewtonModel;
import com.gbaldu.fractal.view.pane.FractalPane;
import com.gbaldu.fractal.view.pane.JuliaPane;
import com.gbaldu.fractal.view.pane.NewtonPane;

import javafx.application.Application;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.KeyCombination;
import javafx.stage.Stage;

/**
 * The MainView class is the entry point of the application and serves as its only window
 */
public class MainView extends Application
{
    /**
     * The initial wiDth of the window
     */
    private static final int WIDTH = 1280;
    /**
     * The initial height of the window
     */
    private static final int HEIGHT = 720;
    /**
     * The root container for the stage
     */
    private Group root;
    /**
     * Gives whether to program is in fullscreen mode or not
     */
    private boolean isFullScreen = false;
    /**
     * The view of the fractal
     */
    private FractalPane fractalView;
    /**
     * The model which represents the fractal
     */
    private FractalModel model;
    /**
     * The controller associated with the model
     */
    private FractalController controller;
    /**
     * The main stage of the window
     */
    private Stage stage;
    /**
     * The represented fractal type; used with a binding to know when to switch displays
     */
    private ObjectProperty<FractalType> fractalType = new SimpleObjectProperty<>();

    /**
     * Used to start the Application, contains all the initialisations
     *
     * @param primaryStage The stage which will accommodate our scene
     * @throws Exception Any Exception that was not caught
     */
    @Override
    public void start(Stage primaryStage) throws Exception
    {
        ///Global setup
        this.stage = primaryStage;
        root = new Group();

        Scene scene = new Scene(root, WIDTH, HEIGHT);
        stage.setTitle("fractal - Dumartin Basile - S3C");
        stage.setScene(scene);
        stage.sizeToScene();
        stage.setMinWidth(600);
        stage.setMinHeight(400);

        stage.setFullScreenExitHint("");
        stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);

        stage.show();

        model = new FractalModel(FractalType.MANDELBROT);
        controller = new MandelBrotController(model);
        fractalView = new FractalPane(model, controller);
        fractalView.init((int) stage.getWidth(), (int) stage.getHeight());
        fractalType.bindBidirectional(model.fractalTypeProperty());
        root.getChildren().add(fractalView);

        ///initialisation of the fractal switcher
        fractalType.addListener((observable, oldValue, newValue) ->
        {
            switch (newValue)
            {
                case MANDELBROT:
                    root.getChildren().remove(fractalView);
                    controller.stop();
                    model = new FractalModel(newValue);
                    controller = new MandelBrotController(model);
                    fractalView = new FractalPane(model, controller);
                    fractalType.bindBidirectional(model.fractalTypeProperty());
                    fractalView.init((int) stage.getWidth(), (int) stage.getHeight());
                    root.getChildren().add(fractalView);
                    break;
                case JULIA:
                    root.getChildren().remove(fractalView);
                    controller.stop();
                    model = new JuliaModel(newValue);
                    controller = new JuliaController(model);
                    fractalView = new JuliaPane(model, controller);
                    fractalType.bindBidirectional(model.fractalTypeProperty());
                    fractalView.init((int) stage.getWidth(), (int) stage.getHeight());
                    root.getChildren().add(fractalView);
                    break;
                case NEWTON:
                    root.getChildren().remove(fractalView);
                    controller.stop();
                    model = new NewtonModel(newValue);
                    controller = new NewtonController(model);
                    fractalView = new NewtonPane(model, controller);
                    fractalType.bindBidirectional(model.fractalTypeProperty());
                    fractalView.init((int) stage.getWidth(), (int) stage.getHeight());
                    root.getChildren().add(fractalView);
                    break;
            }
        });

        ///Fullscreen mode setup
        root.setOnKeyPressed(event ->
        {
            switch (event.getCode())
            {
                case F11:
                    if (isFullScreen)
                    {
                        isFullScreen = false;
                        stage.setFullScreen(false);
                        fractalView.init((int) stage.getWidth(), (int) stage.getHeight());
                    } else
                    {
                        isFullScreen = true;
                        stage.setFullScreen(true);
                        fractalView.init((int) stage.getWidth(), (int) stage.getHeight());
                    }
                    break;
            }
        });

        ///Resizable window setup
        stage.widthProperty().addListener((obs, oldVal, newVal) ->
        {
            if (stage.getWidth() > 0 && stage.getHeight() > 0)
            {
                fractalView.init((int) stage.getWidth(), (int) stage.getHeight());
                if (fractalView.sphere != null)
                    fractalView.sphere.init((int) stage.getWidth() - FractalPane.MENU_WIDTH, (int) stage.getHeight());
            }

        });
        stage.heightProperty().addListener((obs, oldVal, newVal) ->
        {
            if (stage.getWidth() > 0 && stage.getHeight() > 0)
            {
                fractalView.init((int) stage.getWidth(), (int) stage.getHeight());
                if (fractalView.sphere != null)
                    fractalView.sphere.init((int) stage.getWidth() - FractalPane.MENU_WIDTH, (int) stage.getHeight());
            }
        });

        ///Initialisation of the closing actions (here, close the program)
        stage.setOnCloseRequest(event -> System.exit(0));
    }
    
    public static void main(String[] args) {
        launch(args);
    }
}