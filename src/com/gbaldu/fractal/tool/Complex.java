package com.gbaldu.fractal.tool;

/**
 * The Complex class represents real Complex number.
 * It is necessary to do the calculations of the complex-based fractals this program implements.
 */
public class Complex
{
    /**
     * The two parts of the imaginary number.
     */
    public double real, imaginary;

    /**
     * Basic constructor for a Complex.
     *
     * @param real      Initial value for the real part of the Complex
     * @param imaginary Initial value for the imaginary part of the Complex
     */
    public Complex(double real, double imaginary)
    {
        this.real = real;
        this.imaginary = imaginary;
    }

    /**
     * Compares this instance of Complex to the object given.
     *
     * @param obj The object to compare with
     * @return True if the two objects have the same imaginary and real values
     */
    @Override
    public boolean equals(Object obj)
    {
        if (obj.getClass() == Complex.class)
            if (((Complex) obj).real == real && ((Complex) obj).imaginary == imaginary)
                return true;
        return super.equals(obj);
    }

    /**
     * Adds to the real part of this Complex.
     *
     * @param real The value to add
     * @return A new Complex which has the updated values
     */
    public Complex plus(double real)
    {
        return new Complex(this.real + real, imaginary);
    }

    /**
     * Adds two Complex together.
     *
     * @param other The Complex to add
     * @return A new Complex which has the updated values
     */
    public Complex plus(Complex other)
    {
        return new Complex(real + other.real, imaginary + other.imaginary);
    }

    /**
     * Adds to the real part of this Complex.
     * Changes its own values instead of returning real new instance.
     *
     * @param real The value to add
     */
    public void selfPlus(double real)
    {
        this.real = this.real + real;
    }

    /**
     * Adds two Complex together.
     * Changes its own values instead of returning real new instance.
     *
     * @param other The Complex to add
     */
    public void selfPlus(Complex other)
    {
        real = real + other.real;
        imaginary = imaginary + other.imaginary;
    }

    /**
     * Subtracts to the real part of this Complex.
     *
     * @param real The value to subtract
     * @return A new Complex which has the updated values
     */
    public Complex minus(double real)
    {
        return new Complex(this.real - real, imaginary);
    }

    /**
     * Subtracts a Complex to this instance.
     *
     * @param other The Complex to subtract
     * @return A new Complex which has the updated values
     */
    public Complex minus(Complex other)
    {
        return new Complex(real - other.real, imaginary - other.imaginary);
    }

    /**
     * Subtracts to the real part of this Complex.
     * Changes its own values instead of returning real new instance.
     *
     * @param real The value to subtract
     */
    public void selfMinus(double real)
    {
        this.real = this.real - real;
    }

    /**
     * Subtracts a Complex to this instance.
     * Changes its own values instead of returning real new instance.
     *
     * @param other The Complex to subtract
     */
    public void selfMinus(Complex other)
    {
        real = real - other.real;
        imaginary = imaginary - other.imaginary;
    }

    /**
     * Multiplies to the real part of this Complex.
     *
     * @param real The value to multiply by
     * @return A new Complex which has the updated values
     */
    public Complex multiply(double real)
    {
        return new Complex(this.real * real, imaginary * real);
    }

    /**
     * Multiplies two Complex together.
     *
     * @param other The Complex to multiply by
     * @return A new Complex which has the updated values
     */
    public Complex multiply(Complex other)
    {
        return new Complex(real * other.real - imaginary * other.imaginary, real * other.imaginary + other.real * imaginary);
    }

    /**
     * Multiplies to the real part of this Complex.
     * Changes its own values instead of returning real new instance.
     *
     * @param real The value to multiply by
     */
    public void selfMultiply(double real)
    {
        this.real *= real;
        imaginary *= real;
    }

    /**
     * Multiplies two Complex together.
     * Changes its own values instead of returning real new instance.
     *
     * @param other The Complex to multiply by
     */
    public void selfMultiply(Complex other)
    {
        double tmp = real * other.real - imaginary * other.imaginary;
        imaginary = real * other.imaginary + other.real * imaginary;
        real = tmp;
    }

    /**
     * Divides the real part of this Complex.
     *
     * @param real The value to divide by
     * @return A new Complex which has the updated values
     */
    public Complex divide(double real)
    {
        return new Complex(this.real / real, imaginary / real);
    }

    /**
     * Divides this Complex by another.
     *
     * @param other The Complex to divide by
     * @return A new Complex which has the updated values
     */
    public Complex divide(Complex other)
    {
        return new Complex((real * other.real + imaginary * other.imaginary) / other.modulusPow(), (imaginary * other.real - real * other.imaginary) / other.modulusPow());
    }

    /**
     * Divides the real part of this Complex.
     * Changes its own values instead of returning real new instance.
     *
     * @param real The value to divide by
     */
    public void selfDivide(double real)
    {
        this.real /= real;
        imaginary /= real;
    }

    /**
     * Divides this Complex by another.
     * Changes its own values instead of returning real new instance.
     *
     * @param other The Complex to divide by
     */
    public void selfDivide(Complex other)
    {
        double tmp = (real * other.real + imaginary * other.imaginary) / other.modulusPow();
        imaginary = (imaginary * other.real - real * other.imaginary) / other.modulusPow();
        real = tmp;
    }

    /**
     * Multiplies this Complex by himself to emulate a pow() operation
     *
     * @param power The number of times the Complex will be multiplied, and the desired pow
     * @return A new Complex with the correct value
     */
    public Complex pow(int power)
    {
        Complex z = clone();
        Complex tmp = clone();
        for (int i = 2; i <= power; i++)
            z.selfMultiply(tmp);
        if (power > 0)
            return z;
        return null;
    }

    /**
     * Multiplies this Complex by himself to emulate a pow() operation
     * Changes its own values instead of returning real new instance.
     *
     * @param power The number of times the Complex will be multiplied, and the desired pow
     */
    public void selfPow(int power)
    {
        Complex tmp = clone();
        for (int i = 2; i <= power; i++)
            selfMultiply(tmp);
    }

    /**
     * Gives the conjugate of this Complex
     * @return A new Complex which represents the conjugate
     */
    public Complex conjugate()
    {
        return new Complex(real, -imaginary);
    }

    /**
     * Gives the conjugate of this Complex
     * Changes its own values instead of returning real new instance.
     */
    public void selfConjugate()
    {
        imaginary = -imaginary;
    }

    /**
     * Gives the absolute value of this Complex
     *
     * @return The magnitude of this Complex
     */
    public double modulus()
    {
        return Math.sqrt(real * real + imaginary * imaginary);
    }

    /**
     * Gives the absolute value of this Complex, squared
     *
     * @return The magnitude of this Complex, squared
     */
    public double modulusPow()
    {
        return real * real + imaginary * imaginary;
    }

    /**
     * Clones this instance of Complex.
     *
     * @return A new instance of Complex with the same values as this one
     */
    @Override
    public Complex clone()
    {
        return new Complex(real, imaginary);
    }
}
