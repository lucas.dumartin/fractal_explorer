package com.gbaldu.fractal.controller;

import com.gbaldu.fractal.model.FractalType;

import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

/**
 * The IFractal interface gives all the methods to implement for the computation of a fractal.
 */
interface IFractal
{
    /**
     * Initialisation of the controller
     * This method has to reset all the computation variables
     *
     * @param width  The width of the image to calculate
     * @param height The height of the image to calculate
     */
    void init(int width, int height);

    /**
     * Initialises the colors which will be used to draw the fractal.
     */
    void initColor();

    /**
     * Calculates the color of a pixel based off the results of the computation and the max iterations.
     * @param x The position of the pixel along the X axis
     * @param y The position of the pixel along the Y axis
     * @param maxIterations The maximum number of iterations used for this computation
     * @return The Color of the pixel.
     */
    Color getColor(int x, int y, double maxIterations);

    /**
     * Launches the computation of the fractal.
     */
    void launchCalc();

    /**
     * Launches the computation of the fractal.
     * The computation is delayed
     *
     * @param ms The delay, in milliseconds
     */
    void launchCalcIn(int ms);

    /**
     * Stops any computation operations.
     */
    void stop();

    /**
     * Regenerates an image based on the model data.
     */
    void reloadModel();

    /**
     * Changes the maximum number of iterations for the computation.
     *
     * @param value The offset to change the max iterations by
     */
    void changeMaxIterations(double value);

    /**
     * Moves the center of the image to generate to a point.
     *
     * @param valueX The center of the image to generate along the X axis
     * @param valueY The center of the image to generate along the Y axis
     */
    void moveTo(double valueX, double valueY);

    /**
     * Moves the center of the image to generate by X and Y.
     *
     * @param x The position of the center along the X axis
     * @param y The position of the center along the Y axis
     */
    void moveToOffset(double x, double y);

    /**
     * Sets the image to save the computation result.
     *
     * @param wi The image which will receive the computation results.
     */
    void setWritableImage(WritableImage wi);

    /**
     * Sets whether calculations for a zoom are in progress.
     * @param value Whether calculations are in progress
     */
    void setIsZooming(boolean value);

    /**
     * Sets the scale (zoom) factor for the next calculation.
     * @param value The new value to use as scale factor
     */
    void setScale(double value);

    /**
     * Changes the type of fractal that this controller will generate.
     * Actually only used as a switch to trigger controller/model replacement.
     * @param type The new fractal type
     */
    void changeFractalType(FractalType type);
}
