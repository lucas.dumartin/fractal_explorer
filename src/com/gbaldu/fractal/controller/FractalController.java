package com.gbaldu.fractal.controller;

import java.math.BigDecimal;
import java.util.Vector;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import com.gbaldu.fractal.model.FractalModel;
import com.gbaldu.fractal.model.FractalType;
import com.gbaldu.fractal.tool.Complex;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.util.Duration;

/**
 * The FractalController class is used as a container to store the fractal Controllers.
 * It implements IFractal.
 */
public abstract class FractalController implements IFractal
{
    /**
     * The model associated with this controller
     */
    protected FractalModel fractalModel;
    /**
     * Vector used to contain the threads
     */
    private Vector<Thread> threads = new Vector<>();
    /**
     * A threadsafe Boolean to know when to stop the threads
     */
    private AtomicBoolean Interrupted = new AtomicBoolean(false);
    /**
     * The timeline to use for delayed calculations
     */
    private Timeline timeline;

    /**
     * Constructor that must be used when creating a new instance of FractalController
     *
     * @param fractalModel The model to be associated with this controller
     */
    public FractalController(FractalModel fractalModel)
    {
        this.fractalModel = fractalModel;

        fractalModel.calculatedColumns = new ConcurrentLinkedQueue<>();

        fractalModel.setMaxIterations(100.);

        fractalModel.setScale(300);
        fractalModel.setCenter(0., 0.);

        initColor();
    }

    /**
     * The actual computation of the fractal.
     * Calculates whether the pixel belongs to the fractal or not
     *
     * @param x             The position of the pixel along the X axis
     * @param y             The position of the pixel along the Y axis
     * @param maxIterations The number of iterations desired
     * @param interrupted   Serves to know whether the computation has been aborted or not
     */
    protected abstract void calculate(int x, int y, double maxIterations, AtomicBoolean interrupted);

    /**
     * Initialisation of the fractal.
     * Initialises the image and the arrays.
     *
     * @param width  The width of the image to generate
     * @param height The height of the image to generate
     */
    @Override
    public void init(int width, int height)
    {
        fractalModel.writableImage = new WritableImage(width, height);

        stop();
        fractalModel.setSize(width, height);

        fractalModel.iterations = new int[fractalModel.getWidth()][fractalModel.getHeight()];
        fractalModel.results = new Complex[fractalModel.getWidth()][fractalModel.getHeight()];

        launchCalcIn(50);
    }

    /**
     * Initialises the colors which will be used to draw the fractal.
     */
    @Override
    public void initColor()
    {
        fractalModel.colors = new Color[360];
        for (int j = 0; j < 360; j++)
        {
            int i = (j + 240) % 360;
            double sat = Math.atan(Math.abs(Math.sin(6 * i * Math.PI / 180.))) / 1.5;
            double bri = 1;
            if (i % 360 < 30)
            {
                //bri = Math.pow((i - 15) % 30, 2) / 225.;
                sat = 0;
            } else
                bri = 1;
            fractalModel.colors[j] = Color.hsb(i, sat, bri);
        }
    }

    /**
     * Calculates the color of a pixel based off the results of the computation and the max iterations.
     * Has to be
     *
     * @param x             The position of the pixel along the X axis
     * @param y             The position of the pixel along the Y axis
     * @param maxIterations The maximum number of iterations used for this computation
     * @return The Color of the pixel.
     */
    public abstract Color getColor(int x, int y, double maxIterations);

    /**
     * Launches the computation of the fractal.
     */
    @Override
    public void launchCalc()
    {
        launchCalcIn(300);
    }

    /**
     * Launches the computation of the fractal.
     * The computation is delayed
     *
     * @param ms The delay, in milliseconds
     */
    @Override
    public void launchCalcIn(int ms)
    {
        if (timeline != null)
            timeline.stop();
        timeline = new Timeline(new KeyFrame(
                Duration.millis(ms),
                ae ->
                {
                    initFractalComputation();
                    timeline = null;
                }
        ));
        timeline.play();
    }

    /**
     * Setup of the fractal computation.
     * This method resets the results containers, and calls stopThreads
     * It then creates Lines, and starts them.
     */
    private void initFractalComputation()
    {
        if (fractalModel.verbose)
            System.out.println("x:" + fractalModel.getCenterX() + " \ny:" + fractalModel.getCenterY() + "\nScale:" + fractalModel.getScale());

        fractalModel.setGenerationStatus(0.);

        fractalModel.calcTime = new AtomicLong(0);
        fractalModel.drawTime = 0;
        fractalModel.calculatedColumns.clear();
        fractalModel.renderStartTime = System.currentTimeMillis();
        if (fractalModel.isZooming)
            fractalModel.isZooming = false;
        int cores = Runtime.getRuntime().availableProcessors();
        if (fractalModel.verbose)
            System.out.println("Il y a " + cores + " Threads de disponibles");

        stopThreads();

        AtomicBoolean tmpBoolean = Interrupted;
        for (int i = 1; i < cores + 1; i++)
        {
            Line l;
            l = new Line(i, cores, fractalModel.writableImage, tmpBoolean);
            threads.add(l);
            l.start();
        }
    }

    /**
     * Stops computation of the fractal by stopping all the threads.
     * Flushes the threads container, and the current computation results container.
     */
    private void stopThreads()
    {
        Interrupted.set(true);
        if (!threads.isEmpty())
            for (Thread th : threads)
                th.interrupt();
        Interrupted = new AtomicBoolean(false);
        fractalModel.calculatedColumns = new ConcurrentLinkedQueue<>();
        threads.clear();
        fractalModel.nbLinesCalculated = 0;
    }

    /**
     * Stops any computation operations.
     */
    @Override
    public void stop()
    {
        stopThreads();
    }

    /**
     * Regenerates an image based on the model data.
     */
    @Override
    public void reloadModel()
    {
        fractalModel.setMaxIterations(100.);
        fractalModel.setScale(300);
        fractalModel.setCenter(0.,0.);
        launchCalcIn(50);
    }

    /**
     * Changes the maximum number of iterations for the computation.
     *
     * @param value The offset to change the max iterations by
     */
    @Override
    public void changeMaxIterations(double value)
    {
        if ((fractalModel.getMaxIterations() + value) > 5)
            fractalModel.setMaxIterations(fractalModel.getMaxIterations() + value);
    }

    /**
     * Moves the center of the image to generate to a point.
     *
     * @param valueX The center of the image to generate along the X axis
     * @param valueY The center of the image to generate along the Y axis
     */
    @Override
    public void moveTo(double valueX, double valueY)
    {
        fractalModel.setCenter(fractalModel.getCenterX() + valueX, fractalModel.getCenterY() + valueY);
    }

    /**
     * Moves the center of the image to generate by X and Y.
     *
     * @param x The position of the center along the X axis
     * @param y The position of the center along the Y axis
     */
    @Override
    public void moveToOffset(double x, double y)
    {
        fractalModel.addToCenter(x, y);
    }

    private void Draw(int column, AtomicBoolean interrupted, WritableImage wi)
    {
        if (!interrupted.get())
        {
            fractalModel.nbLinesCalculated++;
            BigDecimal percentage = BigDecimal.valueOf(fractalModel.nbLinesCalculated + 1).divide(BigDecimal.valueOf(fractalModel.getWidth()),
                    1, BigDecimal.ROUND_UP);
            fractalModel.setGenerationStatus(percentage.doubleValue());
            if (fractalModel.nbLinesCalculated == fractalModel.getWidth()) {
                fractalModel.setGenerationStatus(1.);
                fractalModel.totalRenderTime = System.currentTimeMillis() - fractalModel.renderStartTime;
                fractalModel.setGenerationStatistics(
                        "Total : "
                                + fractalModel.totalRenderTime
                                + "ms\n"
                                + "   - Calcul : "
                                + fractalModel.calcTime
                                + "ms\n" + "   - Draw : "
                                + fractalModel.drawTime
                                + "ms");
            }
        }
        int line = 0;
        PixelWriter pw = wi.getPixelWriter();
        double maxIterations = fractalModel.getMaxIterations();
        while (!interrupted.get() && line < wi.getHeight() - 1)
        {
            try {
                pw.setColor(column, line, getColor(column, line, maxIterations));
                // pw.setColor(column, line, fractalModel.colors[((360 - 16 * c % 360) % 360)]);//Color.hsb(i, sat, bri));
            } catch (ArrayIndexOutOfBoundsException e) {
                //Cas de l'ecriture durant un resize avec des Runlaters qui n'ont aps réussi a s'arreter a temps
                System.err.println("Error : ArrayIndexOutOfBoundsException : Access to iterations on (" + column + "; " + line + ") impossible." + wi.getWidth());
            }catch (IndexOutOfBoundsException e) {
                //Cas de l'ecriture durant un resize avec des Runlaters qui n'ont aps réussi a s'arreter a temps
                System.err.println("Error : Access to iterations on (" + column + "; " + line + ") impossible." + wi.getWidth());
            }
            line++;
        }
    }

    /**
     * The RunLater method is used to draw the results on the image
     * @param wi The image to draw on
     * @param interrupt Interruption switch
     * @param calculatedColumns Container for the computed results
     */
    private void RunLater(final WritableImage wi, AtomicBoolean interrupt, ConcurrentLinkedQueue<Integer> calculatedColumns)
    {
        Platform.runLater(() ->
        {
            int maxDrawPerRunLater = 16;
            while (!calculatedColumns.isEmpty() && !interrupt.get() && maxDrawPerRunLater > 0)
            {
                long tmpTime = System.currentTimeMillis();
                Draw(calculatedColumns.poll(), interrupt, wi);
                if (!interrupt.get())
                    fractalModel.drawTime += System.currentTimeMillis() - tmpTime;
                maxDrawPerRunLater--;
            }
        });
    }

    /**
     * Sets the image to save the computation result.
     *
     * @param wi The image which will receive the computation results.
     */
    @Override
    public void setWritableImage(WritableImage wi)
    {
        fractalModel.writableImage = wi;
    }

    /**
     * Sets whether calculations for a zoom are in progress.
     * @param value Whether calculations are in progress
     */
    @Override
    public void setIsZooming(boolean value)
    {
        fractalModel.isZooming = value;
    }

    /**
     * Sets the scale (zoom) factor for the next calculation.
     *
     * @param value The new value to use as scale factor
     */
    @Override
    public void setScale(double value)
    {
        fractalModel.setScale(value);
    }

    /**
     * Changes the type of fractal that this controller will generate.
     * Actually only used as a switch to trigger controller/model replacement.
     *
     * @param type The new fractal type
     */
    @Override
    public void changeFractalType(FractalType type)
    {
        fractalModel.setFractalType(type);
    }

    /**
     * The Line class is used to compute a column of pixels
     */
    private class Line extends Thread
    {
        /**
         * A boolean to know whether to stop generation or not
         */
        private AtomicBoolean interrupted;
        /**
         * The id of this Line
         */
        private int id;
        /**
         * The number of Cores available on the machine this program runs on
         */
        private int nbCores;
        /**
         * The image to draw onto
         */
        private WritableImage wi;
        /**
         * A threadsafe container to store the computed results
         */
        private ConcurrentLinkedQueue<Integer> calculatedColumns;

        /**
         * The constructor for a Line.
         * @param id The identification number of this Line
         * @param nbCores The number of cores on this computer
         * @param wi The image to draw on
         * @param interrupted The interruption switch
         */
        Line(int id, int nbCores, WritableImage wi, AtomicBoolean interrupted)
        {
            calculatedColumns = fractalModel.calculatedColumns;
            this.id = id;
            this.nbCores = nbCores;
            this.wi = wi;
            this.interrupted = interrupted;
        }

        /**
         * This method is called immediately upon construction of the Line
         */
        public void run()
        {
            long tmpTime = System.currentTimeMillis();
            if (fractalModel.verbose)
                System.out.println("Thread " + id + " Started.");

            int halfWidth = (int) (wi.getWidth()/2.);
            int halfHeight = (int) (wi.getHeight()/2.);

            int j = 0;
            int x = id + (j * nbCores) - halfWidth - 1;

            double maxIterations = fractalModel.getMaxIterations();


            do {
                if (x < halfWidth) {
                    for (int y = -halfHeight; y < halfHeight; y++) {
                        calculate(x, y, maxIterations, interrupted);
                    }
                    calculatedColumns.add(x + halfWidth);
                    RunLater(wi, interrupted, calculatedColumns);
                }
                j++;
                x = id + (j * nbCores) - halfWidth - 1;
            } while (j * nbCores < wi.getWidth() && !isInterrupted());

            tmpTime = System.currentTimeMillis() - tmpTime;
            if (!isInterrupted() && tmpTime > fractalModel.calcTime.get())
                fractalModel.calcTime.set(tmpTime);
            else if (isInterrupted() && fractalModel.verbose)
                System.out.println("Thread Interrupted");
        }
    }
}
