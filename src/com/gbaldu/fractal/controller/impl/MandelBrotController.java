package com.gbaldu.fractal.controller.impl;

import java.util.concurrent.atomic.AtomicBoolean;

import com.gbaldu.fractal.controller.FractalController;
import com.gbaldu.fractal.model.FractalModel;
import com.gbaldu.fractal.tool.Complex;

import javafx.scene.paint.Color;

public class MandelBrotController extends FractalController
{
    /**
     * Constructor that must be used when creating a new instance of MandelbrotController
     *
     * @param fractalModel The model to be associated with this controller
     */
    public MandelBrotController(FractalModel fractalModel)
    {
        super(fractalModel);
    }

    /**
     * The actual computation of the Mandelbrot fractal.
     * Calculates whether the pixel belongs to the Mandelbrot set or not
     *
     * @param x             The position of the pixel along the X axis
     * @param y             The position of the pixel along the Y axis
     * @param maxIterations The number of iterations desired
     * @param interrupted   Serves to know whether the computation has been aborted or not
     */
    @Override
    protected void calculate(int x, int y, double maxIterations, AtomicBoolean interrupted)
    {
        ///Initialisation
        Complex c, z;
        double xCenter = fractalModel.getCenterX();
        double yCenter = fractalModel.getCenterY();
        c = new Complex(x / fractalModel.getScale() + xCenter, y / fractalModel.getScale() + yCenter);
        z = new Complex(0, 0);

        ///Actual calculation
        int i = 0;
        do {
            z.selfMultiply(z);
            z.selfPlus(c);
            i++;
        } while (z.modulusPow() < 4 && i < maxIterations);

        try {
            if (!interrupted.get()) {
                fractalModel.iterations[x + fractalModel.getHalfWidth()][y + fractalModel.getHalfHeight()] = i - 1;
                fractalModel.results[x + fractalModel.getHalfWidth()][y + fractalModel.getHalfHeight()] = z;
            }
        } catch (Exception e) {
            System.err.println("ERROR : Tentative d'acces a la coord (" + (x + fractalModel.getHalfWidth()) + ";" + (y + fractalModel.getHalfHeight()) + "), sur un tableau de " + fractalModel.iterations.length + " sur " + fractalModel.iterations[0].length);
        }
    }

    /**
     * The coloration of the fractal.
     * Uses the model's color offset to obtain different colors.
     * @param x The position of the pixel to color along the X axis.
     * @param y The position of the pixel to color along the Y axis.
     * @param maxIterations The maximum number of iterations
     * @return The color of the pixel of coordinates x,y
     * @throws ArrayIndexOutOfBoundsException When trying to color a pixel which is not in the image
     */
    @Override
    public Color getColor(int x, int y, double maxIterations) throws ArrayIndexOutOfBoundsException
    {
        int ite = fractalModel.iterations[x][y];
        if (ite != maxIterations - 1)
            return fractalModel.colors[((fractalModel.getColorOffset() * ite % 360) % 360)];

        return Color.BLACK;
    }
}