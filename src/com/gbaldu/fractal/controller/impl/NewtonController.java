package com.gbaldu.fractal.controller.impl;

import java.util.concurrent.atomic.AtomicBoolean;

import com.gbaldu.fractal.controller.FractalController;
import com.gbaldu.fractal.model.FractalModel;
import com.gbaldu.fractal.model.NewtonModel;
import com.gbaldu.fractal.tool.Complex;

import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class NewtonController extends FractalController
{
    /**
     * Constructor that must be used when creating a new instance of NewtonController
     *
     * @param fractalModel The model to be associated with this controller
     */
    public NewtonController(FractalModel fractalModel)
    {
        super(fractalModel);

        NewtonModel newtonModel = (NewtonModel) fractalModel;
        fractalModel.setScale(250);
        fractalModel.setMaxIterations(20.);
        newtonModel.setAlpha(1);
        newtonModel.setBeta(0);
    }

    /**
     * The actual computation of the Newton fractal.
     * Calculates whether the pixel belongs to the Julia set or not
     *
     * @param x             The position of the pixel along the X axis
     * @param y             The position of the pixel along the Y axis
     * @param maxIterations The number of iterations desired
     * @param interrupted   Serves to know whether the computation has been aborted or not
     */
    @Override
    protected void calculate(int x, int y, double maxIterations, AtomicBoolean interrupted)
    {
        double tolerance = .1;
        int result = -1;

        double scale = fractalModel.getScale();
        double xCenter = fractalModel.getCenterX();
        double yCenter = fractalModel.getCenterY();

        NewtonModel fractalModel = ((NewtonModel) this.fractalModel);
        int complexity = fractalModel.getComplexity();

        Complex alpha = new Complex(fractalModel.getAlpha(), fractalModel.getBeta());

        Complex z = new Complex(x / scale + xCenter, y / scale + yCenter);

        int i = 0;
        Complex dif;
        while (i < maxIterations) {
            i++;
            Complex fonc = z.clone();
            fonc.selfPow(complexity);
            fonc.selfMinus(1);
            Complex deriv = z.clone();
            deriv.selfPow(complexity - 1);
            deriv.selfMultiply(complexity);
            fonc.selfDivide(deriv);
            fonc.selfMultiply(alpha);
            z.selfMinus(fonc);
            int j = 0;
            do {
                dif = z.minus(fractalModel.getNewtonsPoints(j));
                if (dif.modulusPow() < tolerance) {
                    result = j;
                }
                j++;
            } while (j < complexity && result != j);
            if (result != -1)
                break;
        }

        if (result == -1)
            result = complexity - 1;
        if (!interrupted.get()) {
            fractalModel.rootResults[x + fractalModel.getHalfWidth()][y + fractalModel.getHalfHeight()] = result;
            fractalModel.iterations[x + fractalModel.getHalfWidth()][y + fractalModel.getHalfHeight()] = i;
        }
    }

    /**
     * Initialisation of the fractal.
     * Initialises the image, and the root results.
     *
     * @param width  The width of the image to generate
     * @param height The height of the image to generate
     */
    @Override
    public void init(int width, int height)
    {
        fractalModel.writableImage = new WritableImage(width, height);

        stop();
        fractalModel.setSize(width, height);

        fractalModel.iterations = new int[fractalModel.getWidth()][fractalModel.getHeight()];
        fractalModel.results = new Complex[fractalModel.getWidth()][fractalModel.getHeight()];

        ((NewtonModel) fractalModel).rootResults = new int[width][height];
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                ((NewtonModel) fractalModel).rootResults[i][j] = 0;


        launchCalcIn(50);
    }

    /**
     * Initialisation of the fractal's colors.
     */
    @Override
    public void initColor()
    {
        fractalModel.colors = new Color[360];
        for (int j = 0; j < 360; j++) {
            fractalModel.colors[j] = Color.hsb(j, 1, 1);
        }

    }

    /**
     * The coloration of the fractal.
     * Uses the model's color offset to obtain different colors.
     *
     * @param x             The position of the pixel to color along the X axis.
     * @param y             The position of the pixel to color along the Y axis.
     * @param maxIterations The maximum number of iterations
     * @return The color of the pixel of coordinates x,y
     * @throws ArrayIndexOutOfBoundsException When trying to color a pixel which is not in the image
     */
    @Override
    public Color getColor(int x, int y, double maxIterations) throws ArrayIndexOutOfBoundsException
    {
        NewtonModel fractalModel = (NewtonModel) this.fractalModel;
        int res = fractalModel.rootResults[x][y];
        int ite = fractalModel.iterations[x][y];

        if (ite == maxIterations)
            return Color.BLACK;

        if (res < 0)
            res = 0;

        return fractalModel.colors[(fractalModel.getColorOffset() * (res * 120 + ite % 120)) % 360];

    }
}







