package com.gbaldu.fractal.controller.impl;

import java.util.concurrent.atomic.AtomicBoolean;

import com.gbaldu.fractal.controller.FractalController;
import com.gbaldu.fractal.model.FractalModel;
import com.gbaldu.fractal.model.JuliaModel;
import com.gbaldu.fractal.tool.Complex;

import javafx.scene.paint.Color;

/**
 * The JuliaController inherits some functionality from the abstract class FractalController,
 * but implements the computation a fractal.
 * The JuliaController must be associated to a JuliaModel which contains the necessary parameters as properties.
 */
public class JuliaController extends FractalController
{
    /**
     * Constructor that must be used when creating a new instance of JuliaController
     *
     * @param fractalModel The model to be associated with this controller
     */
    public JuliaController(FractalModel fractalModel)
    {
        super(fractalModel);
    }

    /**
     * The actual computation of the Julia fractal.
     * Calculates whether the pixel belongs to the Julia set or not
     * @param x The position of the pixel along the X axis
     * @param y The position of the pixel along the Y axis
     * @param maxIterations The number of iterations desired
     * @param interrupted Serves to know whether the computation has been aborted or not
     */
    @Override
    protected void calculate(int x, int y, double maxIterations, AtomicBoolean interrupted)
    {
        ///Initialisation
        Complex c, z;
        double xCenter = fractalModel.getCenterX();
        double yCenter = fractalModel.getCenterY();
        z = new Complex(x / fractalModel.getScale() + xCenter, y / fractalModel.getScale() + yCenter);
        c = new Complex(((JuliaModel) fractalModel).getAlpha(), ((JuliaModel) fractalModel).getBeta());

        ///Actual calculation
        int i = 0;
        do {
            z.selfMultiply(z);
            z.selfPlus(c);
            i++;
        } while (z.modulusPow() < 4 && i < maxIterations);

        try {
            if (!interrupted.get()) {
                fractalModel.iterations[x + fractalModel.getHalfWidth()][y + fractalModel.getHalfHeight()] = i - 1;
                fractalModel.results[x + fractalModel.getHalfWidth()][y + fractalModel.getHalfHeight()] = z;
            }
        } catch (Exception e) {
            System.err.println("ERROR : Tentative d'acces a la coord (" + (x + fractalModel.getHalfWidth()) + ";" + (y + fractalModel.getHalfHeight()) + "), sur un tableau de " + fractalModel.iterations.length + " sur " + fractalModel.iterations[0].length);
        }
    }

    /**
     * The coloration of the fractal.
     * Uses the model's color offset to obtain different colors.
     * @param x The position of the pixel to color along the X axis.
     * @param y The position of the pixel to color along the Y axis.
     * @param maxIterations The maximum number of iterations
     * @return The color of the pixel of coordinates x,y
     * @throws ArrayIndexOutOfBoundsException When trying to color a pixel which is not in the image
     */
    @Override
    public Color getColor(int x, int y, double maxIterations) throws ArrayIndexOutOfBoundsException
    {

        Complex result = fractalModel.results[x][y];
        int ite = fractalModel.iterations[x][y];

        if (result != null && ite < maxIterations - 1) {
            double mu = ite + 1 - Math.log(Math.log(result.modulus()) / Math.log(2)) / Math.log(2.);//ite + 1 - log(log(result.modulus())) / log(2.);

            Color c1 = fractalModel.colors[((360 - (fractalModel.getColorOffset() * (ite)) % 360) % 360)];
            Color c2 = fractalModel.colors[((360 - (fractalModel.getColorOffset() * (ite + 1)) % 360) % 360)];
            Color c = c1.interpolate(c2, (mu - (double) ite));

            if (ite != maxIterations - 1) {
                return c;
            }
        }
        ///ite equals maxIterations
        return Color.BLACK;
    }
}